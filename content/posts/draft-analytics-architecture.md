---
title: "Reasoning about data architecture for analytics"
date: 2019-10-05T08:47:11+01:00
draft: true
---

## Background

Consider a stateful app persisting data to point-in-time storage, typically an ACID-compliant [OLTP](https://database.guide/what-is-oltp/) store like PostgreSQL. As the business grows, its data architecture needs to evolve to support analytical as well as transactional workloads. However, analysts and developers tend to have different needs: the first don't know a priori what they want, they need max flexibility, don't care about an additional 500ms latency on a query, and they need both historical and timely data. Basically, they need an [OLAP](https://en.wikipedia.org/wiki/Online_analytical_processing) store, possibly one that allows them to do more transformations at query runtime ([more ELT and less ETL](https://blog.panoply.io/etl-vs-elt-the-difference-is-in-the-how)). Devs, on the other hand, care about latency, serve the end-users, and need to think well about their schemas. So what's the big deal? It's designing a system that delivers timely, and consistent data in a scalable and evolvable way (see more below). It's these different needs that make data science hard. This article is my best shot at establishing a common ground to reason about data architectures. It is in no way exhaustive, please comment if you have thoughts or find some parts confusing or misleading :D.

<!-- ## TL;DR

One way to achieve loose coupling, high consistency and scalability is to use a log-based data streaming platform. Often this requires major architectural changes, and a team of software engineers. If the application doesn't require streaming,

-   Data Freshness (sync every 30 minutes instead of 24 hours)
-   Assume control of the warehouse (views and tables for faster iterations)
-   Ingest all business data into the Data Lake, linked to a Data Warehouse to enable data exploration, and selection of useful data.
-   Decouple processing and transformation (dedicated tool)
-   Decouple processing and reporting to improve visualisations (dedicated tool) -->

## Total Order Broadcast

One of the key concepts when thinking about data architectures is [total order broadcasting](https://en.wikipedia.org/wiki/Atomic_broadcast). Where there is no causal link between events, the lack of total order is not a big problem since concurrent events can be ordered arbitrarily. In logistics, order matters. If A and B are two parcel events that happened in the order A then B, you would like that everyone consuming those events agrees that it was A,B and not B,A. In most cases, constructing a totally ordered log requires all events to pass through a single leader node that decides on the ordering. If the throughput of events is greater than a single machine can handle, you need to partition the log across multiple machines. The order of events in two different partitions is then ambiguous. If the servers are spread across multiple geographically distributed data centers for availability, you typically need to have a separate leader in each datacenter, because network delays make synchronous cross center coordination inefficient.

## Architectural building blocks

### Delivery and Ingestion

Data delivery is the set of pipes and connectors that distribute information to all stakeholders (internal and external). Some might need it to check their parcel status, others to analyze costs, improve a marketing campaign, or reduce churn. Key challenges in data delivery are:

-   consistency: making sure a query returns the same result if run twice)
-   evolvability (it should be easy to change evolve the schema or content of a payload to reflect changes in the business requirements
-   timeliness: we want to know how things are going now, not two days ago
-   scalability: if the business grows 100x we want to be able to keep using the same architecture. Upgrading parts as we go is fine, what we should avoid is the point of no return "shit, we hit a fundamental limit and we cannot scale anymore unless we rewrite the entire codebase")
-   durability: no "oops we lost some data because one of the servers was down"
-   availability: we never want to say "we cannot serve requests for data"

### Transformation

Raw data is what comes from the stateful app (events or states), and from all external sources (Hubspot, Stripe, Zendesk, Google Analytics and more). Here `data transformations` means the set of operations that define a metric, what it means for the business (how it changes in relation to user or system behavior). A metric can be derived from existing metrics, or directly from raw data.

**What are the hard problems in data transformation?**

-   version control (metrics should be versioned to welcome experiments and iterations)
-   fine-grained user access (some things should be kept under a tighter control)
-   tracing how a metric is calculated from the raw data (or other metrics) should be transparent
-   testing to ensure figures match (think finance needs)
-   performance autoscaling (we don't want to worry about the underlying infrastructure that needs to be deployed to crunch numbers at scale)

### Serving

Reporting/Interactive Notebooks, Caches (Tableau, Jupyter, Redis, etc.). With “serving” we mean the set of operations that create a visual representation of a table or a group of metrics and allow to share that with all relevant stakeholders to drive business-critical decisions.

# Ingestion

Data ingestion is generally done with one or more Extract Transform and Load (ETL) pipeline. ETLs exist in both Batch and Stream flavors and can be self-hosted or managed services.

## Batch ETL (Self-hosted)

In the Python world, [Airflow](https://airflow.apache.org/) is the tool to author ETL pipelines (think of it as cron on steroids). There are obviously other task management tools out there but listing them all is out of scope. I focus on Airflow because I have direct experience deploying it in both AWS and GCP, and the general principles apply to other schedulers as well. Airflow is about task management, it worries about _what needs to happen when and in what order._

What Airflow does not do is resource management, decide which core or which node in a cluster should do the job. This is the job of resource management tools, such as [Apache Spark](https://spark.apache.org/) or [Dask](https://dask.org/), with various types of queues, and executors. It also interfaces directly with container orchestration tools such as [Kubernetes](https://kubernetes.io/), so that it's possible to execute a task in a pipeline inside a dedicated pod running a customized docker image. This also means that the actual code that runs inside the image can be arbitrary (could be bash). It uses a PostgreSQL or MySQL db to persist the state of each task, has retry logic built-in, and all of this is specified in code (Python) so pipelines can be version controlled and tested, all inside the same Python repo.

### **How would a batch ETL pipeline look like (say with Airflow)?**

Say we want to replicate and keep synched our OLTP and our OLAP. We would write a pipeline in Airflow that is triggered every time something in our OLTP changes. Once we find a change we append the new information into our warehouse (say Snowflake or BigQuery).

### **Problems with batch ETL**

-   Data inconsistency. An analyst might join two tables that are updated at different times giving rise to a beat frequency. Basically the query result for the same time window might change depending on when the query is run. Not nice.
-   Say the data volume increases and the Airflow executor runs out of RAM. Too bad, you need to upgrade the server.
-   Say the throughput increases and the executor cannot keep up. You need to deploy the app in a server with more workers, to parallelize things more. However, once the ingestion is over these workers will sleep and you will burn money for nothing.

### Horizontal Scaling

There is a way to enable horizontal scaling, but it requires deploying in a container orchestration service (Kubernetes for Airflow) so that the scheduler and the executor (two demons that run in the background) run in two different pods, and every other task launches a new pod to run. Thus, if you need to be 50x faster, you just spawn 50 containers in 50 pods and things go faster. All of this is not easy (I tried). Even if you succeed, you still have a batch processing system, so there is always going to be some inconsistencies.

## Batch ETL (Managed)

Airflow is available as a managed service in GCP under the name of [Cloud Composer](https://cloud.google.com/composer/). When I trialed it in March 2019 I was not satisfied. While it naturally uses Kubernetes under the hood to scale horizontally, you cannot deploy your own containers :(. As a result, CI-CD and automated testing are hard, and environment control is very limited. My conclusion at the time was: unless your workloads are simple you are going to feel tight soon. There are many other managed services out there, we will investigate more once we identify the correct paradigm.

## Streaming ETL (Managed)

### Stitch

Stitch is built on open source. Integrations are powered by Singer, an open-source ETL standard. Redshift users can create their own integrations with a standard JSON-based format and run them seamlessly in Stitch or on their own hardware. <https://aws.amazon.com/redshift/partners-detail/stitch/>. Under the hood, Stitch uses Amazon [S3 and Apache Kafka](https://www.stitchdata.com/blog/stitch-data-pipeline/).

# Processing Architectures

In the Big Data world the two competing models I am aware of are Lambda and Kappa. There are obviously many differences, but philosophically it boils down to:

-   Lambda separates batch computations from live processing, so that the batch can focus on large historical records, and the live branch on delivering high-throughput views. Each uses a different language and tool set.
-   Kappa thinks maintaining two repos for the same change creates barriers and tries to solve this by unifying stream and batch processing under a single framework.

They both come with opportunities and challenges.

### Lambda

Figure from Nathan Martz book "[Big Data: Principles and best practices of scalable realtime data systems](https://www.amazon.com/Big-Data-Principles-practices-scalable/dp/1617290343)". Typically data is saved in a data lake and consumed by two different pipelines, a batch and a fast one. The idea is that all data processing happens in batches using a growing "master dataset", where large-scale distributed processing is typically done with Hadoop MapReduce. The serving layer (after processing is complete) will then have data synced up to the execution time of the last batch. Downstream, results will then need to be updated to take into account new data.

**Pros of lamda**

-   The batch layer of Lambda architecture manages historical data with fault tolerant distributed storage which ensures low possibility of errors even if the system crashes.
-   It is a good balance of speed and reliability, because you use each system for what it's good at.

**Cons of lamda**

-   It results in coding overhead (languages for fast, batch, and serving layers, are typically different (for example Elixir, Python/R/Spark/Hadoop, Javascript).
-   Any change means you need to submit and review two PRs (one for batch, one for fast layer)
-   A data modeled with Lambda architecture is difficult to migrate or re-organize.
-   The two outputs need to be merged, in order to respond to user requests. For a simple aggregation over a tumbling window this is easy, but it becomes significantly harder if the view is derived using more complex operations such as joins and window filters, or if the output is not a time series.

## Kappa

In 2014 *Jay Kreps* published a blog post [questioning the lambda architecture](https://www.oreilly.com/ideas/questioning-the-lambda-architecture). Linkedin's [liquid](https://blog.acolyer.org/2015/02/04/liquid-unifying-nearline-and-offline-big-data-integration/) is an example of the implementation of kappa. The core idea is to unify batch and streaming processing so that the data is processed once in real-time. To achieve this at scale, Kappa uses an **immutable**, **distributed**, **append-only** log ([Apache Kafka](https://kafka.apache.org/)). If you strip it out of all the jargon, you can think of it as huge and **immutable** **slack channel** (immutable = messages cannot be edited in place). Writes happen when there is something to write, multiple destinations can read from it, each at their own pace, as well as scroll back to re-sync with what's been said (*replay*). Slack did introduce Kafka in their architecture with interesting [lessons learned](https://slack.engineering/scaling-slacks-job-queue-687222e9d100) and some insights on the types of failure testings one would need to do.

### Pros **of Kappa**

-   With [KSQL](https://www.confluent.io/product/ksql/), everyone can access what they want/need
-   Because a persistent log is the central piece of the architecture, different domains are decoupled and consistency is easier to achieve than in lambda
-   The persisted append-only store acts as a buffer and thus allows replays

### **Cons of Kappa**

-   Complete set of data over a window that exceeds the buffer size is a pain.
-   Not ideal when algorithms need to be different in batch vs streaming.

## Why is Kafka cool?

1.  Kafka is a persistent storage medium with strong ordering and reliability guarantees (information is saved in a cluster with redundancy baked in). Basically once you put a piece of data in a Kafka topic running in a cluster you are pretty much guaranteed that it won't be corrupted or even worse get lost.
2.  Kafka is essentially a database where data and changes to the data is seen as an ordered stream of events. A conventional database (OLTP or OLAP) is a closed system that expects to be the final destination. If data is to empower every business decision it also needs to be delivered consistently and reliably at scale.
3.  Kafka achieves **total order broadcast** with the immutable append-only log. (Note this is exactly what happens inside a traditional OLTP database, which is why it is often said that Kafka effectively [turns the database inside out](https://www.youtube.com/watch?time_continue=1&v=fU9hR3kiOK0)). To achieve total order broadcast without an append-only log you need to use distributed transactions, such as Atomic Commit and Two-Phase Commit (2PC) which work well inside a circuit board with consistent latency, but don't scale when you have multiple actors across the Internet.
4.  With small systems constructing a totally ordered event log is entirely feasible (OLTP with single-leader replication). As systems are scaled out, lack of consensus undermines total ordering.
5.  It decouples the task of delivering a piece of data across the system from the task of consuming it (Index, Cache, OLTP, OLAP, Data Lake). This is a huge deal, and it's worth thinking about it twice. Without an append-only persistent log, consistency across the network cannot be achieved as the probability that at least one of the two-way handshakes fails (two-phase commits) increases exponentially with the number of such links (basically the number of sinks). It might sound trivial, but it's worth to keep in mind that we are talking about eventual consistency here, not strict consistency (think [more BASE than ACID](https://en.wikipedia.org/wiki/Eventual_consistency)).
6.  It offers a buffer to replay events and thus welcome changes and improvements, which is key for any evolutionary architecture. Evolution effectively stops when making a change (requires hours of dev time).
7.  When you couple it with a unified stream/batch processing framework such as [Apache Beam](https://beam.apache.org/), and its managed brother in GCP called [dataflow](https://cloud.google.com/dataflow/).

## Ok ok, but how is an app using Kafka look like?

[What is Apache Kafka?](<https://youtu.be/06iRM1Ghr1k>)

## How to choose a managed Kafka service?

Confluent wrote an interesting article to help spot [the difference between a partially-managed Kafka service and a fully managed one](https://www.confluent.io/blog/fully-managed-apache-kafka-service). Built-in support for schema management (versioning, upgrades etc). For example, _Confluent Schema Registry_ supports Avro (a serialization format which guarantees both forward and backward compatibility) and integrates directly with Kafka, so that there is no need to provide another data storage for the schema.

## Things to keep in mind about Kafka

-   Data is partitioned by primary key and in Kafka, anything in a partition is guaranteed to be ordered.
-   Choosing the partition key is generally the hard part
-   Self hosting Apache Kafka is HARD! Likely there are many companies providing it as a service, the top one being Confluent.

### Why go through the trouble of Kafka, just use SQS or SNS

SQS is a queue, SNS a notification service with the concept of topics you subscribe to. SQS [destroys the message once it is processed from its queue](https://sookocheff.com/post/aws/comparing-kinesis-and-sqs/) (produce once, consume once) - bye bye Total Order Broadcast (there might be total order, but you lost the broadcast). Kafka is a persistent message broker and allows you to replay events in the log (produce once, consume multiple times). Kafka has strong order guarantees despite being distributed across many nodes in a cluster. Also, this [stack exchange question](https://stackoverflow.com/questions/16449126/kafka-or-sns-or-something-else) is useful.

## Apache Beam

### Why is Beam cool?

Because it unifies batch and streaming processing, basically batch is just a special case of streaming (fixed large windows). To train a statistical or a machine learning model you often need historical data, over a time window that can go from weeks to months. when you are serving the output of a model you want to provide fresh data, nobody cares if a month ago at 1:00 AM the traffic spiked. A different processing framework for these use cases slows down experimentation and testing. Beam solves this.

### Pain Points of Beam

-   Java SDK is much more supported than Python SDK, data scientists work in Python and it would mean having codebases in multiple languages to support.
-   When the bulk of the aggregations run against BigQuery, Apache Beam doesn’t add much value.
-   Analysts and data scientists tend to speak in SQL, translating SQL logic to Python or Java for Beam comes at a cost.
