---
title: "Who are data geeks?"
subtitle: ""
date: 2020-04-27T13:30:23+10:00
featured: false
draft: True
tags:
  - career in data
  - data science
  - data enrichment
  - dataset building
---

**Questions**

>1. *How do data profiles change with seniority?*
>2. *Is there any difference between a roles across countries?*
>3. *What are the top skills and how do they change with seniority?*
>4. *What's the average tenure as a function of seniority?*
>5. *How does company size distributes for each seniority level*
>6. *What is the industry type with a higher number of CDOs?*
>7. *How does the probability of a career progression change with the candidate ambition?*
>8. *What is the data maturity of each company?*

## Building The Dataset

The main source of data for this project is LinkedIn, which I scraped with an automation tool called [Phantombuster](https://phantombuster.com/). At the time of writing they offer a 14-day trial, limited to 1h per day. Everything starts with a search.

![](draft-who-are-data-geeks-ed237.png)

## Enrichment

**Basic Company Data**
[Companies Dataset from People Data Labs](https://www.peopledatalabs.com/company-dataset)
[GlassDoor job listings](https://www.kaggle.com/andresionek/data-jobs-listings-glassdoor#glassdoor.csv)
[Company Reviews - Kaggle](https://www.kaggle.com/venkataganesh/company-reviews)
