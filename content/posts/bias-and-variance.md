---
title: "Bias, Variance, and Ensembles"
subtitle: ""
date: 2020-05-18T14:00:00+10:00
featured: false
draft: false
---

Depending on the context, I think of **determinism** as an *abstraction*, a *useful lie*, or a *model* of reality. Whether you look at the uncertainty principle in quantum mechanics, the outcome of a future football match, or the mutation of a strain of coronavirus, there is always an element of randomness at play.

However, it is useful to make a distinction between the underlying random process, from the observables we can measure. In the sports example, the game is the random process at play, and the score is what we measure. Given a large number of variables at play, we would be unlikely to observe the same outcome if we were to replay the same match over and over.

For an electron in an atom the random process is the interaction with its surrounding matter, and what we measure might be the light absorbed or emitted by virtue of the [photoelectric effect.](https://phet.colorado.edu/en/simulation/photoelectric)

## Key Points

1. Everything in nature can be seen as a **stochastic** process;
2. Prediction errors have an irreducible component called **noise**;
3. The reducible part is made of **bias** and **variance**;
4. Reducing bias typically increases variance and vice versa (**the trade-off**);
5. This has nothing to do with the Heisenberg uncertainty principle;
6. **Ensemble** methods can help compromise bias and variance.


## Prediction error

The prediction error or cost function is what we typically want to minimise. Generally speaking, the error is some function of the distance **d** between elements of the feature vector and the decision boundary (or regression curve).

If the function is linear, each distance contributes linearly to the overall error. If we choose a quadratic function, we will speak of squared error which tends to penalise larger errors proportionally more than smaller errors. If we average it out across all data points we will then speak of **mean square error** or **(MSE)**.

We could also go a step further and measure cubic error (to the power of 3), quartic error (power of 4), or use an exponential error. Here we'll focus on the MSE because of its popularity.

The MSE [can be decomposed](https://stats.stackexchange.com/questions/204115/understanding-bias-variance-tradeoff-derivation) in three terms: **noise**, **bias** and **variance**.

- **Noise** represents the intrinsic (and irreducible) variability of the quantity we are observing. A common example of noise is the black-and-white overlay in photos shot at night, called [luminance noise.](https://www.cambridgeincolour.com/tutorials/image-noise-2.htm)
- **Bias** (or training error) measures how far off are the model predictions from the correct value in the training set. More complex models tend to have a smaller bias, as they fit the data better.
- **Variance** (or generalisation error) explains the prediction variability when the model is fit to different training data. Variance error is inversely proportional to generalisability and it typically increases with model complexity.

I found a good visual representation in the notes of [Scott Fortmann Roe](http://scott.fortmann-roe.com/docs/BiasVariance.html), which decompose error in bias and variance, showing that we can reduce the first by making our models more complex.

![Modelling Gotchas](/images/modelling-gotchas-617ca.png)

For a decision tree classifier, this might be the number of leaves or the number of nodes, for a random forest the number of trees, for K-Nearest-Neighbour (KNN) the number of neighbours, for a neural network the number and size of its layers.

Another way to reduce bias (not shown here) is feature engineering, which typically reduces the algorithm complexity [at the expenses of the data preparation pipeline.](https://towardsdatascience.com/the-power-of-feature-engineering-b6f3bb7de39c?source=rss----7f60cf5620c9---4)

An exception to the bias-variance tradeoff is double-descent which [emerges](https://arxiv.org/abs/1812.11118) in convolutional neural networks: above a certain complexity threshold, both bias and variance drop [simultaneously.](https://openai.com/blog/deep-double-descent/)

## Evaluating stimators

If high scores on training datasets are not guaranteed to survive on unseen data:

> *how can we fairly measure the performance of an estimator?*

The standard way is to calculate training and testing scores on two well-isolated portions of the dataset. The price to pay is a smaller sample-size available for training, which will most certainly lead to [poorer performance.](https://stackoverflow.com/questions/4617365/what-is-a-learning-curve-in-machine-learning) Unfortunately we cannot fix this.

The same applies during the training phase of the model (think of hyper-parameter tuning). We could split the training data into actual training and validation, but we would further reduce the size of the training set. A smarter way to do this is **cross validation**, here is the recipe:

1. divide the training dataset into K segments called *folds*
2. randomly choose 1 fold for validation, and train on the rest
3. record the training and validation scores
4. repeat from 2. until all folds are used for validation exactly once

This has the advantage to use all the data available for training (albeit not at the same time) and to produce statistics on the score such as average and standard deviation. When sampling allows duplicates (with replacement) is called [bootstrap.](https://datascience.stackexchange.com/questions/32264/what-is-the-difference-between-bootstrapping-and-cross-validation)

I like the visual representation of K-CV in by [Mariia Fedotenkova](https://www.researchgate.net/figure/A-schematic-illustration-of-K-fold-cross-validation-for-K-5-Original-dataset-shown_fig5_311668395) reported below.

![Cross Validation visualised](/images/cv-visual.png "From the PhD thesis of Mariia Fedotenkova")

## Ensemble Methods

Increasing the model complexity reduces its ability to perform well on unseen data. One way to improve this is to distribute the prediction task across multiple nodes, an approach that goes under the name of **ensemble** estimation.

The key idea is **diversity gain**: carefully combining information from different channels increases your ability to detect the signal from the noise. Example of this are everywhere, from cellular networks (MIMO communications), to the Global Positioning System (GPS), to neural networks where the output of each node in a layer is a function of some or all of the nodes in the previous layer. We also use diversity gain when we receive a diagnosis from more than one doctor, or when we hire a cross-functional team.

[Bootstrap Aggregating](https://www.stat.berkeley.edu/~breiman/bagging.pdf) (or *Bagging*) is an example of averaging (or voting) strategy proposed by Leo Breiman in 1994. Predictions are made on many datasets obtained by uniformly sampling the original dataset with replacement (i.e. random selection with duplicates). When the base estimator is a decision tree you get [random forests.](https://link.springer.com/article/10.1023/A:1010933404324)

[Boosting](https://en.wikipedia.org/wiki/Boosting_(machine_learning)) algorithms strategically combine a set of weak hypothesis to create a single strong hypothesis, instead of relying on simple averaging as in bagging. Many simple additive models are trained sequentially, where downstream models focus on the limitations of the ones upstream. The evolution is not dissimilar to that of an open-source project made of small incremental improvements. Alexander Galkin [writes](https://stats.stackexchange.com/questions/18891/bagging-boosting-and-stacking-in-machine-learning):
> *In Boosting the subset creation is not random and depends upon the performance of the previous models: every new subset contains the elements that were (likely to be) misclassified by previous models.*

Boosting was first demonstrated to work in 1990 by Robert E. Schapire in a paper titled [the strength of weak learnability.](https://link.springer.com/article/10.1007/BF00116037) Seven years later in 1997, Schapire and Yoav Freund had the idea weighing hypothesis proportionally to their accuracy and published the [AdaBoost](https://www.sciencedirect.com/science/article/pii/S002200009791504X) paper. Schapire [writes](http://rob.schapire.net/papers/explaining-adaboost.pdf) that the algorithm is essentially a procedure of [greedy](https://en.wikipedia.org/wiki/Greedy_algorithm) minimisation of the exponential loss, while implicitly implementing an L1 (lasso) form of regularisation. The implicit L1 regularisation is a neat idea and perhaps explains why AdaBoost performs so well in practice.

The link between boosting and gradient descent was established later in 1999 by [J. H. Friedman](https://projecteuclid.org/download/pdf_1/euclid.aos/1013203451), showing support for any differentiable loss function.

Around 2015 [gradient tree boosting](https://scikit-learn.org/stable/modules/ensemble.html#gradient-boosting) gained popularity, and stimulated the development of a portable library [XGBoost](https://github.com/dmlc/xgboost/tree/master) to perform boosting efficiently on distributed machines.

Today XGBoost is an open-source project with more than 400 contributors, and is considered the top-performing algorithm for inference on tabular datasets with numbers and categories.

### Closing notes

The irreducibility of bias and variance might sound similar to the Heisenberg uncertainty principle in Quantum Mechanics, and some proposed to call it [the uncertainty-principle of cross-validation.](http://www.ise.bgu.ac.il/faculty/mlast/papers/01635796.pdf)

I am glad the community did not adopt this name. The bias-variance trade-off comes from classical probability theory and represents our ignorance of the true function f(**x**) that links features to targets. This means that if only we knew the right function, we would be able to reduce both errors to zero.

The Heisenberg uncertainty principle is about irreducible ignorance. For a sub-atomic particle, the product of the position and momentum uncertainties (or equivalently of their energy and observation time) is at least equal to half the Plank's constant. There is nothing we can ever do to get around this - it's a fundamental lower bound.

There are still parts of some official package documentation that are poorly worded. An example from [scikit-learn](https://scikit-learn.org/stable/modules/learning_curve.html):

> *The bias of an estimator is its average error for different training sets. The variance of an estimator indicates how sensitive it is to varying training sets. Noise is a property of the data.*

Here *varying* and *different* could be misinterpreted as synonyms. The way I interpret the first sentence is that the measure of how well a model learned the patterns in the training set is typically an average on different training sets.

Another great paragraph on the bias-variance trade-off is on the [XGBoost docs](https://xgboost.readthedocs.io/en/latest/tutorials/model.html#objective-function-training-loss-regularization), a big thank you goes to the authors for the colourful graphics.

I hope you enjoyed this post! If you have suggestions or thoughts leave a comment below or reach out on your preferred social media platform.
