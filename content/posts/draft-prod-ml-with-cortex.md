---
title: "ML into production with Cortex"
subtitle: "Kubernetes Cluster one yaml file away."
date: 2020-04-29T11:00:00+10:00
featured: false
draft: true
tags:
  - production
  - cortex
  - kubernetes
  - accidental complexity
---

Serving ML models in production is not hard per se, but there are many moving parts and a lot of knowledge required to setup, deploy, monitor, and maintain the backend. Let's see how complicated it can be.

At a bare minimum, you need to spin up a Web Server that accepts HTTP requests and define the API, which is the run-time contract between the server and the rest of the world. Let's say you write a [flask app](https://realpython.com/flask-by-example-part-1-project-setup/) running on an instance of [EC2](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/concepts.html) or [Compute Engine](https://cloud.google.com/compute/?utm_source=google&utm_medium=cpc&utm_campaign=japac-AU-all-en-dr-bkws-all-super-trial-e-dr-1008074&utm_content=text-ad-none-none-DEV_c-CRE_248263937473-ADGP_Hybrid+%7C+AW+SEM+%7C+BKWS+~+T1+%7C+EXA+%7C+Compute+%7C+1:1+%7C+AU+%7C+en+%7C+google+compute-KWID_43700023244271230-kwd-312950839106&userloc_9071833&utm_term=KW_google%20compute&gclid=EAIaIQobChMI8LbV87-M6QIVmSQrCh0RcwhzEAAYASAAEgJPt_D_BwE), and protect your resources from unauthorised users with [authentication strategies](https://cloud.google.com/docs/authentication) and [security rules](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-security-groups.html).

>What happens if the load spikes?

Well, you might want to deploy your models in a cluster with an [autoscaling policy](https://cloud.google.com/compute/docs/autoscaler) so that you can keep serving requests no matter what. Ok cool.

> How do you make sure each node appears identical to any other?

Ah yeah, that's easy with [Docker](https://docs.docker.com/). *Just* write a Docker file and use that to deploy the same image to each node.

> How are you going to build your containers?

As part of a Continuous Integration-Continuous Delivery (CI-CD) framework (Jenkins, Circle-CI, GitLab). Tagged commits containing your source code and all the dependencies are automatically built into a binary (the container) conditional to all your tests passing. Ok, containers are cool, but nodes might crush if there is bug in the code, or if they run our of memory.

> How do you monitor their health and discover when new ones become available?

Good point, you might then use a container orchestration tool like [Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/), see [EKS](https://docs.aws.amazon.com/eks/latest/userguide/what-is-eks.html) or [GKE](https://cloud.google.com/kubernetes-engine).

> How do you make sure you are the only one that can reach the machines in the cluster?

With a Virtual Private Network in with services like [VPC](https://docs.aws.amazon.com/vpc/latest/userguide/what-is-amazon-vpc.html) for AWS or [Cloud VPN](https://cloud.google.com/vpn/docs/concepts/overview) for GCP.

> That's cool, but how do you make sure you are using all nodes of the cluster equally instead of one node working hard and the rest idle?

Ah yep, you might also need an application load balancer like [this](https://www.cloudflare.com/lp/load-balancing/?_bt=378710660589&_bk=load%20balancer&_bm=e&_bn=g&_bg=68980464382&_placement=&_target=&_loc=9071833&_dv=c&awsearchcpc=1&gclid=EAIaIQobChMI8f2djLqM6QIVUxaPCh1HwQDPEAAYBCAAEgK8EfD_BwE), AWS [Elastic Load Balancing](https://docs.aws.amazon.com/elasticloadbalancing/latest/application/introduction.html), or Google [Load Balancing](https://cloud.google.com/load-balancing/docs) services.

Overwhelmed? Yep, you are not alone. And we haven't even talked about rolling updates.

## Key Points
Instead of assembling all the parts of car, including road-testing and safety checks, projects like [Cortex](https://github.com/cortexlabs/cortex) give you a functioning car. Cortex hides all of the *accidental complexity* in K8s, with minimal compromises on the [control parameters that matter](https://www.cortex.dev/cluster-management/config). It boils down to three files:

- trainer.py --> to train the ML model
- predictor.py --> to make predictions on unseen data
- cortex.yaml --> to configure the cluster

Cortex uses [Cloudformation](https://aws.amazon.com/cloudformation/), AWS-specific infrastructure as code framework like [Terraform](https://www.terraform.io/). At the time of writing the roadmap includes the support of [other cloud providers](https://gitter.im/cortexlabs/cortex?at=5de76ca7d75ad3721d4f0e62).

## Example

path: /Users/lorismarini/code/loris/cortex-test/sklearn/iris-classifier

Taken from the official tutorial

**After 15 minutes**

`cortex cluster info` returns:
![](draft-prod-ml-with-cortex-b998f.png)

**Quick test**

Installed
https://github.com/yuya-takeyama/ntimes
https://github.com/yuya-takeyama/percentile

And curled the endpoint to see how quickly I can get responses:

```
ntimes 10 -- curl -s -o /dev/null -w '%{time_starttransfer}\n' http://a2c926bcb839f11eab7b106934f8fa91-1742727989.ap-southeast-2.elb.amazonaws.com/iris-classifier \
    -X POST -H "Content-Type: application/json" \
   -d '{"sepal_length": 7, "sepal_width": 1, "petal_length": 8, "petal_width": 0.3}' | percentile
```

What I get is:

```50%:	0.048431
66%:	0.049847
75%:	0.050621
80%:	0.053612
90%:	0.055209
95%:	0.055209
98%:	0.055209
99%:	0.055209
100%:	0.0630759999999999
```

**Cost**

**Monitor API**

## Conclusion
