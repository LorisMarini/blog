---
title: "Misunderstandings"
subtitle: "Obvious tips to minimise misunderstandings"
date: 2020-03-26T13:30:23+10:00
featured: false
draft: true
---

The act of using data to support a positive business outcome is full of communications challenges. Data scientists often work with people with varied backgrounds, from finance to engineering, including analytics and product owners and more.

- Misunderstandings are normal
- The act of listening (language recognition) is pretty random, we are guessing all the time
- Why Misunderstandings occur?
  - How digital communications work
  - Language Barriers
  - Differences in contextual knowledge
  - Cultural barriers (see the idea of high-context/low-context communication by [Erin Meyer -The Culture Map](https://www.goodreads.com/book/show/22085568-the-culture-map))
