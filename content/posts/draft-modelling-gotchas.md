---
title: "Modelling gotchas"
subtitle: ""
date: 2020-03-26T13:30:23+10:00
featured: false
draft: true
---

## Bias Variance tradeoff

In supervised learning the expected mean squared error of an estimator can be decomposed in three terms: **bias**, **variance** and **noise**.

**Noise** is the intrinsic variability in the target variable and is irreducible. The assumption is that there is always an element of randomness at play, and the output fluctuates even when the input is constant. If you are familiar with CCD cameras think of thermal noise, what you see when you take a 2 minutes picture of absolute darkness with max ISO.
**Bias** measures how far off are the model predictions from the correct value in the training set. Bias generally decreases with model complexity, as the model learns to better fit the training set.
**Variance** explains the prediction variability when the model is fit to different training data. Variance error is inversely proportional to generalisability and it typically increases with model complexity.

The contributions to the total error are summarised well by Scott Fortmann Roe [in fig 6 of his essay](http://scott.fortmann-roe.com/docs/BiasVariance.html)

![](modelling-gotchas-617ca.png)

To keep in mind:

- Bias and variance cannot be simultaneously reduced
- More complex models --> Lower Bias, Higher Variance (Overfitting)

There are still parts of some official documentations that are poorly worded. An example is this from scikitlearn:

> The bias of an estimator is its average error for different training sets. The variance of an estimator indicates how sensitive it is to varying training sets. Noise is a property of the data.

Here *varying* and *different* training sets sound like the same thing. What the authors wanted to say in the first sentence is that the measure of how well a model learned the patterns in the training set is typically an average on different training sets.

**Nothing to do with Heisenberg**

Note that the irreducibility of these two terms might sound like the Heisenberg uncertainty principle in Quantum Mechanics. So much so that someone even proposed to name this the [uncertainty-principle of cross-validation](http://www.ise.bgu.ac.il/faculty/mlast/papers/01635796.pdf). However, the two are not related. The bias-variance trade-off comes from classical probability theory and represents our ignorance of the true function f(**x**) that links features to targets. This means that if only we knew the right function, we would be able to reduce both errors to zero. The Heisenberg uncertainty principle is also about ignorance, but at a much more fundamental level. The principle states that given a sub-atomic particle, the product of the uncertainty of its position and momentum is at least equal to half the Plank's constant. In the language of quantum mechanics it's said that these two quantities don't commute, and it similarly applies to energy and time.

#### Reducing Bias

- [Boosting Ensemble Estimators](https://scikit-learn.org/stable/modules/ensemble.html)

#### Reducing Variance

**Model selection**

[model selection](https://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter)

**K-fold cross validation**

The idea here is to divide the training set into groups called *folds*, say k non-overlapping groups. We then train our model on data from k-1 randomly-selected groups, and test it on the single remaining group. We then repeat the procedure so that each group is used as testing once, and calculate an aggregate score (say the average).

The tacit assumption of kfold is that each entry in the dataset is independent from each other, and follows the same (perhaps unknown) distribution. One can break this assumption by introducing memory in the data acquisition process. For example, data might come from a malfunctioning probe whose output drifts repeatably every 10 measurements or so, thus introducing a level of correlation in the observations.

If it's important to have a balanced categorical target, use [stratified kfold instead](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.StratifiedKFold.html).

K-fold cross-validation and boostrap are two types of resampling methods. The first resamples without replacement and thus produces surrogate data sets that are smaller than the original. On the contrary, bootstrap resamples with replacement, leading to possibly duplicated instances, and not guarantee that all original cases are included at least once. See [this stackexchange question](https://datascience.stackexchange.com/questions/32264/what-is-the-difference-between-bootstrapping-and-cross-validation).

**Average Ensemble Estimators**

A way to reduce variance and improve generalisability is to average the predictions of several base estimators. Examples are Bagging, Random forests etc.

## Information Leaks

Information leakage happens when one fails to keep a strong separation between training and testing data. The most common example of this is in the context of feature preprocessing such as normalisation. If we normalise based on the combined training + testing data sets, information will leak from the second into the first. Non-leaky data preparation must happen [within each fold of the cross validation cycle](https://machinelearningmastery.com/data-leakage-machine-learning/). To fix this we should calculate the rescaling parameters based on the training set of each iteration (the randomly-selected k-1 folds) and use this to prepare the data on the single test fold. This also applies to feature selection, feature extraction, and model selection. One way to avoid leaks in sklearn is to make a [pipeline](https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.make_pipeline.html) as Tim Heads shows in his post [cross-validation gone wrong](https://betatim.github.io/posts/cross-validation-gone-wrong/), or this paper on [leaking in data mining](https://www.cs.umb.edu/~ding/history/470_670_fall_2011/papers/cs670_Tran_PreferredPaper_LeakingInDataMining.pdf).

## Curse of Dimensionality

Low-dimensional intuition is useless in high-d.

## Other pitfalls

- Rely on one algorithm
- Project goal: aim at the right target
- Model goal: choose a cost function that makes sense for the problem
- Rely only on data
- Remove outliers blindly. Odd things should make you think...
- Embrace uncertainty and try to estimate it whenever possible!
