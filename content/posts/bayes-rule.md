---
title: "Bayes' Rule"
subtitle: "Visually"
date: 2020-03-29T10:04:00+10:00
featured: false
draft: false
---

Bayes' rule is a one-line theorem which tells us how we should update our initial beliefs about an hypothesis when new evidence becomes available. Its application underpins the entire field of science and is key for many data mining algorithms, from Bayesian classifiers to Bayesian neural networks. The purpose of this short post is to provide an intuition for how this works, using a simplified medical example.

## Scenario

Imagine that there are only 3 diseases in the world, D1, D2 and D3 which occasionally manifest with red spots on the patient's forearm skin. You are a doctor and your patient shows these symptoms. Your job is to calculate the probability that the patent has any or none of the known disease. In general each disease manifests red spots with different rates. In addition, healthy people can occasionally have the same symptoms, for various other reasons. Let's draw this:

![Bayes Rule Example](/images/bayes-rule-b1835.png "D1-D3 are three possible deseases, H represents the healthy population.Symptoms [red] can occur for various reasons, Bayes rule is a nice way to account for all the possibilities.")

where the area of each rectangle is proportional to the number of people in that category, and they all add up to the total number of people (healthy and ill). Starting with the first disease, we want to find the probability that **red spots imply disease 1**.

Let's indicate this with P(D1|spots). The symbol | is not the [unix pipe](https://en.wikipedia.org/wiki/Pipeline_(Unix)), but represents a conditional probability and should be read "given that we observed". The reverse, P(spots | D1) corresponds to probability that **disease 1 leads to red spots** and can be calculated by looking at medical archives.

Note that the act of conditioning the probability corresponds to filtering the dataset to focus on patients that had disease 1. Of all of them:

- D1,spots - **had** red spots
- D1,no-spots - **didn't have** red spots

Note that if we only look at the bold rectangle above, P(spots | D1) = P(spots). The calculation is trivial: P(spots | D1) = D1,spots / D1, which we imagine to be 0.3 (or 30%).

> Can this be the final result?

It would be if everybody had the disease, but we know this isn't the case. A sensible thing to do is to scale this down by the probability that someone develops disease 1 in the first place. This is called **prior** probability and we indicate it simply as P(D1). The word prior here means that this isn't conditioned to other observations, we simply count all the patients with disease 1, and divide it by the total number of people (with or without a disease). Let's assume P(D1) = 0.01 or 1% and recap what we have so far.

Can this be the final answer?

P(D1 | spots) ? P(spots | D1) * P(D1)

If there was no other way to have red spots other than disease 1 this would be the end of our quest. Since this is in general not true, we should **divide** the current result by the probability that people have red spots due to **any reason** and obtain **Bayes' rule**:

P(D1 | spots) = P(spots | D1) * P(D1) / P(spots) = 0.15 or 15%.

where we assumed that P(spots)=0.02.

## Intuition check

One way to understand why we divide by P(spots) is to reason by extremes. If P(spots) is much smaller than 1, the symptom is rare. When we divide a quantity for a number smaller than 1 we effectively magnify its value. We conclude that rare symptoms increase the probability that the patient has disease 1, which makes sense intuitively.

The opposite case is when everyone always has red spots all the time:
- P(spots) = 1
- P(spots | D1) = 1 because spots are always there with or without disease

Given that the observation of red spots doesn't add any information, we are left with P(D1 | spots) = P(D1). It might take a bit of playing around with numbers to appreciate this, but from a first look the theorem seems to make sense.

## Bayes rule in SQL

Let's see how these calculations can be done in SQL. Assume we have a records table, with columns:

- id --> patient id, unique
- disease --> one of [D1, D2, D3, Null]
- has_red_spots --> one of [True, False]

Missing vales in the disease column correspond to healthy patients. With a few CTEs we could write:

{{< highlight SQL >}}
with n_d1 as (
  select
    count(id)
  from record where disease = "D1"
  ),
n_spots as (
  select count(id) from record
  where has_spots is True
  ),
n_d1_spots as (
  select count(id) from record
  where disease = "D1" and has_spots is True
  ),
all as (
  select count(id) from record
  )
select
  (n_d1 / all) as pd1,
  (n_spots / all) as p_spots,
  (n_d1_no_spots / n_d1) as p_spots_given_d1
  (p_spots_given_d1 * pd1) / p_spots as p_d1_given_spots
{{< /highlight >}}

Where p_d1_given_spots is the answer to the question: **What's the chance that someone with red spots has disease 1?**. To find the most likely cause of the symptoms, we should repeat this for each value in the disease column.

## More Reading

- [The theory that would not die](https://www.amazon.com/Theory-That-Would-Not-Die-ebook/dp/B0050QB3EQ)
- [Understanding Bayes Theorem](https://towardsdatascience.com/understanding-bayes-theorem-7e31b8434d4b)
- [The Bayesian Trap](https://www.youtube.com/watch?v=R13BD8qKeTg)

I hope you enjoyed this post! If you have suggestions or thoughts leave a comment below or reach out on your preferred social media platform.
