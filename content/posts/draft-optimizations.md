---
title: "Gradient Descent"
subtitle: ""
date: 2020-02-10T14:00:00+10:00
featured: false
draft: true
---

## Gradient Descent and Stochastic Gradient Descent

http://www.cs.cmu.edu/~pradeepr/convexopt/Lecture_Slides/stochastic_optimization_methods.pdf

## Stochastic Gradient

The most wildly successful class of algorithms for taking advantage of the sum structure for problems where n is very large are stochastic gradient (SG) methods [Robbins and Monro, 1951, Bottou and LeCun, 2003]. A

## Stochastic Average Gradient

http://www.birs.ca/workshops/2014/14w5003/files/schmidt.pdf
https://arxiv.org/abs/1309.2388

## BFGS
Deep dive into how the algorithm works (mathematics)

https://stats.stackexchange.com/questions/284712/how-does-the-l-bfgs-work/285106
