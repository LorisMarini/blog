---
title: "Manage data at scale"
subtitle: ""
date: 2020-04-28T16:01:00+10:00
featured: false
draft: true
---

![](draft-manage-data-at-scale-6690d.png)

[Big Query Access control](https://cloud.google.com/bigquery/docs/access-control#bigquery)
[Controlling access to a BigQuery dataset](https://cloud.google.com/bigquery/docs/dataset-access-controls)


In Confluent Cloud, you can stream data from a Kafka topic into your Data Warehouse of choice, for example [BigQuery](https://docs.confluent.io/current/cloud/connectors/cc-gcp-bigquery-sink.html#cc-gcp-bigquery-sink).

If you need more control, such as streaming by event time and not processing time, or splitting events from the topic into different tables in BigQuery, you might find Apache Beam useful.


> Loading data by event time using Beam is described in details in this post, implemented by windowing the data in event-time semantics, and ingesting it to BigQuery by its windowed event time. I’ll take a different approach, by fetching the event timestamp from the metadata of the record. As for dividing a stream to separate tables, Beam’s DynamicDestinations was designed to do just that, giving you full freedom to choose a destination table for each event. Here’s a code sample achieving both these goals:

![](draft-manage-data-at-scale-636ac.png)

Also see [this post on Apache Beam to stream into BigQuery](https://medium.com/google-cloud/bigquery-partitioning-with-beam-streams-97ec232a1fcc).
