---
title: What is machine learning?
subtitle: The elements of learning.
date: 2020-04-01T06:50:00.000+00:00
featured: false
tags:
- machine learning
- feedback
- cost functions
- regularisation

---
At the time of writing, a Google search for _deep learning_ gives 1.9b results, nearly one for every 3 people on the planet. To navigate the clutter, we need a simple frame of reference to understand machine learning. In this post, I will try to convince you that classical computers are beautifully boring, yet they can learn thanks to four simple concepts: distance, objective, model, and direction.

## Key Points

The fundamental building blocks of a machine learning algorithm relate to these four questions:

* **Objective**: What defines success?
* **Constraints**: Assumptions we make that simplify the problem (e.g. linear relationship)?
* **Distance**: Where are we and how far are we from where we want to go?
* **Direction**: What's the best next move given where we are at?

## Binary machines

Modern CPUs are fine objects. They require the most advanced manufacturing and design capabilities to achieve fast and reliable computations - take a look at this video by [Intel](https://www.youtube.com/watch?v=d9SWNLZvA8g&mute=1) to get an idea. However, at the heart of their enormous complexity is just a bunch of zeros and ones and the ability to retain them (memory), as well as flip and add them up (processing). All other operations are somehow derived. The good news is that they operate at the rate of billions of cycles per second. In comparison, our biological brain works at a frequency of about 100 cycles per second, as the action potential round-trip time is on the order of 10 ms - see [Bostrom Superintelligence](https://www.amazon.com.au/Superintelligence-Nick-Bostrom/dp/1501227742) and [Lovinger 2008.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3860493/)

**Representing reality**

A machine becomes useful when we associate an idea to a collection of bits. For example, we might decide that 0100 represents the decimal number 4, and 0101011101 represents the decimal number 349, and call this binary alphabet. We can also arbitrarily decide that a sequence of three decimal numbers represents a point in 3D, one for each coordinate (x,y,z). Many such triplets could indicate the shape or the trajectory of something in space, from the orbiting satellite that feeds your Pay-Tv data, to my cat strolling lazily around the house. By now we are deep into the woods of computer science.

## Elements of learning

**Ingredient 1: Objective**

Feedback is an essential ingredient in automation and errors can have [fun side effects](https://www.youtube.com/watch?time_continue=6&v=JzlsvFN_5HI&feature=emb_logo&mute=1) or catastrophic side effects [make headlines.](https://www.wired.com/story/uber-self-driving-crash-volvo-polestar-1-roundup/)

Continuous feedback is essential to adapt and improve, from students in a classroom to the knowledge discovery process (data understanding and business understanding are typically pictured in a closed-loop). It might sound obvious, but feedback alone makes no sense without a clear objective. That is, we first should define what "good" means, and then compare the current state to the desired state and take corrective actions as required.

By now we agreed on how to measure distance, so we know how far we need to go and we can quantify the _cost_ (or error) of not being there yet. Since these change from one iteration to another, we speak of _cost functions_.

![Learning feedback loop - Google ML crash course](/images/ml-ingredients-2017b.png "Iterative trial-and-error process that machine learning algorithms use to train a model - Google ML crash course.")

Not surprisingly, the outcome of a machine learning model changes substantially when we adjust what we mean by cost. Sometimes we can leverage this to discourage the algorithm to produce certain results. When this is used to improve the generalisation of the predictions we talk of **regularisation**.

**Ingredient 2: Constraints**

In how many ways we can connect two points A and B? If we lived in a 1D world there would be only one answer. Already in 2D the answer infinite, let alone 3D or ND. Since we cannot deal with infinity, we need to simplify the problem by making assumptions. This is the idea of a **model**, a simplification of reality that embodies our assumptions on what we can do to solve the problem. A simple example is assuming that the target variable (what we want to predict) depends linearly from the dataset features.  

**Ingredient 3: Distance**

How far are two numbers? It turns out there are more than 2000 answers. Why you need so many would require its own textbook, but you can get an idea by skimming this [table of contents](https://link.springer.com/book/10.1007/978-3-642-30958-8) - a bit crazy I know.

However, if we agree on one definition of distance (say Euclidean), and we can represent the world in machine code, we can solve real problems, including how much my cat walks on an average day. Also, the idea of distance is related to the concept of similarity, such as finding similar customers or products (segments) or build recommendation engines.

**Ingredient 4: Direction**

We have a clear goal, we made assumptions and we have a way to track the learning progress. The last bit of the puzzle is a strategy to make the next step, given our current position and the final goal. Perhaps the most famous strategy is gradient descent, or its probabilistic younger brother _Stochastic Gradient Descent - SGD_. The basic idea is to make a move and compare your progress before and after to figure out if we are moving in the right direction. There are many references on this topic. Some of my favourites are:

* [Sebastian Raschka](https://sebastianraschka.com/faq/docs/closed-form-vs-gd.html)
* [MIT OpenCourseWare](https://ocw.mit.edu/courses/mathematics/18-065-matrix-methods-in-data-analysis-signal-processing-and-machine-learning-spring-2018/video-lectures/lecture-22-gradient-descent-downhill-to-a-minimum/)
* [Google ML crash course](https://developers.google.com/machine-learning/crash-course/reducing-loss/stochastic-gradient-descent)


I hope you enjoyed this post! If you have suggestions or thoughts leave a comment below or reach out on your preferred social media platform. Happy inferencing :).
