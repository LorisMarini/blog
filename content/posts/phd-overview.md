---
title: "Lasers in flatland"
subtitle: "My phd research in one page"
date: 2020-03-26T13:30:23+10:00
featured: false
draft: false
---

![](phd-overview-061c4.png)

Quantum computing promises to enable faster and more powerful computation than would ever be possible with a classical CPU. The core idea is to encode information into the quantum state of subatomic particles (electrons or photons). States evolve into an output state which contains the result of the computation.

But there is a catch. These states need to be indistinguishable in a quantum-mechanical sense so that they can be entangled at least over one of their degrees of freedom (polarisation, or energy, or space). In addition, we need to deal with the *no-cloning theorem* of quantum mechanics, which states that it's impossible to make an identical copy of a quantum state.

> If we cannot make copies, how on earth are we going to create indistinguishable states?

### A photocopy machine for quantum states

Since copying is not an option, we could generate a large number of particles and select those with the same features (such as energy, polarisation). This might be a lossy solution, but eventually some of them will be identical.

The idea is cool but... it won't work.
The reason is that identical particles are not necessarily indistinguishable. Indistinguishable means that you cannot tell them apart. There must be no way to say which is which. In the example above, it's evident that two lucky identical photons would always be generated at different times, and thus be distinguishable :-(.

### The wonders of quantum mechanics

As it's often the case, cool things happen in nonlinear regimes. Broadly speaking, a system works nonlinearly when doubling the input doesn't cause the output to double. What does this mean for an atom? When a pulse of light comes in contact with an atom, energy and momentum are exchanged between the light field and the electrons. If the pulse is strong enough, the electrons will respond in a way similar to an over-stretched rubber band: their oscillations saturate, the colour changes and, at some point, they'll "snap". This is cool, but how is this related to indistinguishability?

As it turns out nonlinearities can be observed in either **stimulated** or **spontaneous** experiments. Recipe: for the first we need:

- input: two photons reach the atom at *exactly the same time*
- output: a third photon comes out, whose energy (and momentum) is the sum of the two

The recipe for spontaneous emission is the opposite:

- input: one photon reaches the atom
- output: two photons comes out, the sum of their energy and momentum equal to those of the input photon.

This second example, is what happens in [Spontaneous Parametric Downconversion (SPDC)](https://en.wikipedia.org/wiki/Spontaneous_parametric_down-conversion). What's amazing is that the conservation of energy requires them to be generated at exactly the same time. So we can detect one, and always know that an identical twin is there waiting to be used. Repeat this many times over, and you basically get a **probabilistic** source of quantum indistinguishable photons.

### State of the art and research goal

In most existing platforms light interacts with billions of atoms in optically-nonlinear materials. While this results in high conversion efficiencies, it also constrains the output to be narrow-band and the platform to be inflexible. The root cause is optical phase matching, a condition that ensures that all the particles of light involved travel at the same speed so that they can keep interacting over large distances.

The goal of my PhD was to demonstrate the first experimental observation SPDC in 2D materials. These are structures as thin as they can get (down to the single atom). I looked at a special crystal called *TMDC* which bears the name of the atoms that make up its crystal plane. Why TMDCs?

- highly nonlinear
- easy to fabricate
- chemically stable
- highly configurable optical properties

### Challenges

The unequivocal demonstration of SPDC requires a coincidence measurement. The idea is to separate the identical photons (I used a partially-reflective mirror), detect them independently, and show that the number of simultaneous detections is far larger than chance. It turns out that SPDC is intimately connected to another nonlinear process called second-harmonic generation (SHG), and the efficiency of one process provides insights on the other.

### Outcomes

I led high-power free-space laser experiments to collect evidence of non-resonant SPDC from a diffraction-limited area of a bi-dimensional surface, one photon at a time. I found that the photoluminescence generated during the interaction of light with the 2D surface was the only signal our single-photon detectors picked up. This means that the generation efficiency that on 2D surfaces the light-matter interaction is too weak to allow a successful coincidence measurement.

I couldn't quite build a source of entangled photon pairs at the nanoscale which is sad :(. However, 2D surfaces could be deposited on conventional light waveguides, such as an optical fibre with far greater yield. This would open a range of new possibilities where linear and nonlinear problems are solved separately, and the parts assembled somewhat akin to LEGO blocks. I leave it to the next wave of brave experimentalists to pick up from where I left off.

### Lessons learned

As progress in scientific research advances, questions become more subtle and experiments more challenging. Good science needs great engineering, which is why more than 80% of my 4 years of research were spent building and testing specialised equipment required for such challenging measurements.


I hope you enjoyed this post! If you have suggestions or thoughts leave a comment below or reach out on your preferred social media platform. Happy inferencing :).
