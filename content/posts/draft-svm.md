---
title: "Support Vector Machines"
subtitle: "Linear and interpretable data mining"
date: 2020-03-26T13:30:23+10:00
featured: false
draft: true
tags:
  - machine learning
  - decision trees
  - information
  - entropy
---

Support Vector Machines is powerful because:

- it's guaranteed to find a global maximum (convex space)
- different kernels allow you do distort the space to gain a better prospective
- took 30 years for the scientific community to appreciate the idea of SVM

[MIT lecture by Patrick Winston](https://www.youtube.com/watch?v=_PwhiWxHK8o)

Linear regression, logistic regression, and support vector machines are all very similar instances of our basic fundamental technique: fitting a (linear) model to data. The key difference is that each uses a different objective function.

> Once again, the support-vector machine’s solution is intuitively satisfying. Skipping the math, the idea is as follows. In the objective function that measures how well a particular model fits the training points, we will simply penalise a training point for being on the wrong side of the decision boundary. In the case where the data indeed are linearly separable, we incur no penalty and simply maximise the margin. If the data are not linearly separable, the best fit is some balance between a fat margin and a low total error penalty. The penalty for a misclassified point is proportional to the distance from the decision boundary, so if possible the SVM will make only “small” errors.

> The SVM’s objective function incorporates the idea that a wider bar is better. Then once the widest bar is found, the linear discriminant will be the centre line through the bar (the solid middle line in Figure 4-8). The distance between the dashed parallel lines is called the margin around the linear discriminant, and thus the objective is to maximise the margin.

- training point that are on the wrong side of the decision boundary are penalised proportionally to the distance from the decision boundary
- when a linear boundary exists there is no penalty, we simply maximise the margin
- where there isn't a linear boundary we'll find a balance between a fat margin and a low total error penalty.

http://blog.minitab.com/blog/adventures-in-statistics-2/regression-analysis-how-do-i-interpret-r-squared-and-assess-the-goodness-of-fit
