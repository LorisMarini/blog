---
title: "What I learned from Streamlit"
subtitle: ""
date: 2020-04-29T13:30:23+10:00
featured: false
draft: true
---

[Streamlit](https://github.com/streamlit/streamlit) is a friendly way to build a interactive front-ends for your ML apps with plots from Matplotlib, Plotly, Bokeh and Altair. Alternatives are Panel, Bokeh and Plotly Dash.

In this post we will deploy a simple streamlit app (stateless web app) in a managed server via Heroku, and in fully-managed kubernetes cluster in GCP via their Cloud Run service. These strategies share two goals:

1. reducing deployment complexity
2. controlling the execution environment

The difference in is the way they handle load, concurrency, and billing. In Heroku the application runs in server with the highest availability money can buy. If load increases slowly, we can provision a bigger machine. If load increases abruptly, [backpressure](https://www.tedinski.com/2019/03/05/backpressure.html) will propagate through the system reaching your queues, potentially leading to higher latency and worse customer experience.

In Cloud Run we stop worrying about load because our app is deployed to a fully-managed kubernetes cluster, which scales elastically to zero. This reflects on the cloud bill: compute is charged in increments of 0.1 seconds and we only pay when there are requests to serve. Three aspects make Cloud run sound similar to *serverless* development:

1. stateless
2. autoscaling
3. scale-to-zero

However, there are important differences. In *serverless* the business logic is broken down in isolated stateless functions often offered as a service (FaaS) so that the developer can focus on the business logic instead of worrying about the execution environment. In Cloud Run all our business logic (a streamlit dashboard) is enclosed in a container (monolith) and it executes in an environment we (developers) need to control.

**Key Points**

1. Heroku upgrades come with bumpy cost increases
2. Dynos are designed for web apps, not for memory-hungry applications.


## Streamlit context

Evolution is often a matter of trial and error. Two of the earlier Python plotting libraries are:

- Matplotlib (Started in 2008)
- Seaborn (Started in 2013)

Matplotlib (is written in pure Python and is based on NumPy), while Seaborn builds on top of Matplotlib and is inspired by the R library ggplot2. With web browsers getting more efficient a number of attempts were made to bring data visualisation in the browser. Some of the most notable projects and when they started include:

1. D3.js (2011)
2. Bokeh (Nov 2013)
3. Plotly (Dec 2013)
4. Vega (Nov 2016)
5. Vega-Lite (July 2017)
6. Altair (May 2018)
7. Streamlit (April 2019)

[D3](https://github.com/d3/d3) is a JavaScript library written and maintained by Michael Bostockfor for data visualisation using web standards (SVG, Canvas and HTML). A D3 statement is a document transformation to add or modify elements in response to data. See also the [the project official website](https://d3js.org/), the original [paper published in 2011](http://vis.stanford.edu/papers/d3), and a [presentation by Murray, Cukier and Heer](http://www.jeromecukier.net/presentations/d3-tutorial/S01%20-%20introduction.pdf).

[Plotly](https://github.com/plotly/plotly.py) (Interactive visualisations built on top of D3.js https://en.wikipedia.org/wiki/Plotly)

[Bokeh](https://github.com/bokeh/bokeh) (HTML5 canvas and WebGL, see technical vision https://docs.bokeh.org/en/0.10.0/index.html#technicalvision)

[Vega](https://vega.github.io/vega/about/) was build with one goal:
> *Promote an ecosystem of usable and interoperable tools, supporting use cases ranging from exploratory data analysis to effective communication via custom visualisation design.*

[Vega-Lite](https://vega.github.io/vega-lite/) was created to reduce the verbosity of Vega by a factor of 10, while being able to compile to full-blown Vega specifications.

[Altair](https://github.com/altair-viz/altair) ([example gallery](https://altair-viz.github.io/gallery/index.html)) was born by the frustrations of Jake VanderPlas, which noticed the accidental complexity of existing libraries for browser-based interactive plotting and decided to change this.

Altair is a declarative pythonic layer that generates the json objects needed by Vega-Lite to render plots with D3.js using CSS and SVG. Works started in September 2018, eighteen months later the project saw 100 contributors and 5.1k stars.

- Works in a Jupyter Notebook
- No 3D plots (who needs them really?)
- Seaborn still better for statistical plots

As [Fernando Irarrázaval](http://fernandoi.cl/blog/posts/altair/) writes, Altair is based around three concepts:
- Marks (how we want to represent data points)
- Channels (the mapping between variables and degrees pf freedom on the graph)
- Encoding (the type of variable, date, ordinal or categorical)

It's instructive to read the Twitter thread by the author Jake VanderPlas [on how he felt while building it](https://twitter.com/jakevdp/status/1006929120628916224).

## Deployment in Heroku

Heroku is a platform to deploy server apps and manage the infrastructure on which they run. It includes tools for continuous integration, continuous delivery, and [environment management](https://devcenter.heroku.com/articles/pipelines) to reduce the DevOps work to a minimum.

Heroku makes it easy spin up a managed PostgreSQL database, Redis cache or queue, and even deploy experimental versions of an app from a [feature branch](https://devcenter.heroku.com/articles/github-integration-review-apps). Your code is deployed on lightweight Linux containers called Dynos, which abstract away all the complexity of defining ad orchestrating containers.

**Get ready**

[Install](https://devcenter.heroku.com/articles/heroku-cli#download-and-install) Heroku and login from terminal with `heroku login` (this will start an OAuth flow in your browser) see also their [getting started](https://devcenter.heroku.com/articles/heroku-cli#getting-started) page. Move to directory containing the app to deploy and type

`heroku create`

To create an app and add the heroku remote. Check that the new remote was added with `git remote -v`. When you are ready you can push your local changes with `git push heroku master`.

**Heroku Buildpacks**

The idea is to add the heroku remote to our git repository, and push to that when we are ready to roll out a new version of the app. Heroku automatically creates a new release version when we push new changes, and automatically starts the build/release pipeline.

[Heroku Builpacks](https://devcenter.heroku.com/articles/buildpacks) are...

Each buildpack has minimum requirements, for instance the python one [expects](https://devcenter.heroku.com/articles/deploying-python#expected-files-for-python) one of these files:

- requirements.txt (needed by `pip` to create the right environment for your app)
- setup.py (the file used by `pip` to install your module if you have one)
- Pipfile (if you use `pipenv`)

If none of these files is found, the buildpack will throw an error of the type *"No default language could be detected for this app."* - (we use requirements.txt). Finally we need to write a process file or [Procfile](https://devcenter.heroku.com/articles/preparing-a-codebase-for-heroku-deployment#3-add-a-procfile), to tell Heroku how to start our app. We are now ready to push our changes to the heroku remote

`git push heroku master`

```zsh
git push heroku master
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 310 bytes | 310.00 KiB/s, done.
Total 3 (delta 2), reused 0 (delta 0)
remote: Compressing source files... done.
remote: Building source:
remote:
remote: -----> Python app detected
remote: -----> No change in requirements detected, installing from cache
remote: -----> Installing SQLite3
remote: -----> Installing requirements with pip
remote: -----> Discovering process types
remote:        Procfile declares types -> web
remote:
remote: -----> Compressing...
remote:        Done: 125.2M
remote: -----> Launching...
remote:        Released v6
remote:        https://arcane-gorge-90135.herokuapp.com/ deployed to Heroku
remote:
```

The first 7 lines are the same you would get with any `git push`. What follows is the sequence of operations that Heroku does under the hood to release the new version.

**Heroku Docker**

There is an easy way and a slightly harder way. The first is to specify the pipeline in `heroku.yml`, then set the container as a deployment method with `heroku stack:set container`, commit changes and push them with `git push heroku master`. This is great if you have small images and your build time doesn't bother you (to the best of my knowledge the container image is rebuilt every time as part of the deployment pipeline).

The other way is to define

...........

...........

heroku.yml has 4 top-level sections: setup, build, release and run.

## Deployment in Cloud Run

Cloud Run is a GCP service that abstracts away the complexity required to deploy and secure real-world web apps, with no infrastructure management and a good developer experience.

The inputs are a web app, a Dockerfile, and a cloud region. The logic inside the container image is stateless, and Cloud Run automatically scales the service based on the number of incoming requests. With zero requests the infrastructure also scales to zero and so does your bill. What we own:

1. the app (business logic)
2. the Dockerfile

What google owns:

- elastic scaling with scale-to-zero
- security (TLS)
- concurrency
- load-balancing
- release management
- container registry
- image building and pushing
