---
title: "GSheet to pandas"
subtitle: ""
date: 2020-04-27T13:30:23+10:00
featured: false
draft: True
tags:
---

## Steps

1. Enable Google Drive API in GCP
2. Create a service account file and save it locally (for example in your HOME)
3. Get the id of the google drive folder containing your files
`https://drive.google.com/drive/u/0/folders/<folderid>?ths=true`
4. List all filenames and ids in `<folderid>` with [pydrive](https://gsuitedevs.github.io/PyDrive/docs/build/html/pydrive.html#pydrive.files.GoogleDriveFile.InsertPermission) (authentication via OAuth2.0)
6. Share each file with the service account email address
7. Load each file into a pandas DataFrame using a wrapper to [`pygsheets`](https://github.com/nithinmurali/pygsheets)

**Sharing files with the python library google-api-python-client**

PyDrive supports [updating file permissions](https://gsuitedevs.github.io/PyDrive/docs/build/html/pydrive.html#pydrive.files.GoogleDriveFile.InsertPermission), but to create a permission object it's best to use the underlying [google-api-python-client](https://github.com/googleapis/google-api-python-client/blob/f0b025b95b2fb5fd0369d7233614e35b9c86af30/docs/oauth-installed.md#creating-the-object):

```
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

flow = InstalledAppFlow.from_client_secrets_file(
    'client_secret.json',
    scopes=['https://www.googleapis.com/auth/drive.metadata.readonly'])

# Geenrate credentials via the OAuth2.0 auth flow
credentials = flow.run_local_server(host='localhost',
    port=8080,
    authorization_prompt_message='Please visit this URL: {url}',
    success_message='The auth flow is complete; you may close this window.',
    open_browser=True)

# Get the drive service object
drive_service = build('drive', 'v3', credentials=credentials)
```

Then you can use the code in the [example](https://developers.google.com/drive/api/v3/manage-sharing#python) to specify a new permission policy and update it for each file id.
