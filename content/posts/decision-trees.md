---
title: "What is a decision tree?"
subtitle: "Linear and interpretable data mining"
date: 2020-03-26T13:30:23+10:00
featured: false
draft: false
tags:
  - machine learning
  - decision trees
  - information
  - entropy
---

A decision tree is a collection of nodes connected with edges. Each node represents a **test** on an attribute (e.g a coin-flip), each branch represents an **outcome** (heads or tails). The classification result can be read by walking the tree from the first *root* node to an *edge* node or leaf, which encodes the class label. [George Seif](https://towardsdatascience.com/a-guide-to-decision-trees-for-machine-learning-and-data-science-fe2607241956) writes that a decision tree is:

> (...) a set of sequential, hierarchical decisions that ultimately lead to some final result.

![Cancer decision tree](/images/cancer-tree.jpg "Example built on the Wisconsin Breast Cancer dataset , using just 3 attributes")

Decision tree algorithms are a linear, supervised, non-parametric, highly interpretable, data mining algorithm, used for both classification and regression.

### Information and purity

Before we dive into decision trees, it's best to clarify what is information and how this relates to purity.

The core idea is simple. Imagine we are looking at a laptop screen projecting one letter at a time, and that the first letter is X. If we are absolutely sure that all other letters will also be X, we would get bored pretty quickly. Since all the information we can get from the box is carried by the first observation, we can safely turn off the screen and do something else. However, if all letters displayed are selected at random with the same probability, we have no other option than looking at the screen and recording everything we see, or else we would lose *information*.

Using the language of purity we would say that the output of the first example is *maximally pure*, because the same letter over and over again carries the least amount of information. The second example would be *minimally pure* (carry maximum information).

Information is measured by a quantity called entropy, formally defined for the first time in 1948 by Claude Shannon. Other than being one of the most beloved concepts by physicists, entropy is at the heart of the Internet as we know it today, from compression algorithms to forward error correction in digital communication channels.

- **max purity** --> **min information** --> **entropy = 0**
- **min purity** --> **max information** --> **entropy = 1**

Another measurement of impurity is the Gini index, which is [computationally cheaper](https://datascience.stackexchange.com/questions/10228/when-should-i-use-gini-impurity-as-opposed-to-information-gain).

### Algorithm heuristic

Training a decision tree is a recursive process. At each step, we partition the current group following the values of a categorical variable, and measure the purity of the partitions with respect to the target variable. We repeat this for all attributes in the dataset, and select the one that provides the best split (highest purity). Every time we repeat this, we add a node to the tree, until we run out of variables or decide to stop.

In practice, there will be as many segments as there are distinct values of the current attribute, called children sets. The entropy of the new set with respect to the target variable is simply the sum of the entropy of each child weighted by the proportion of instances belonging to that child (which approximates the probability that the target variable falls into that segment).

Tree-structured models can represent any function of the features with arbitrary precision. However, a tree that grows until its leaves are pure tends to overfit. Using to many features can also lead to overfitting, so care must be taken to find a trade-off between variance and bias. A good way to limit the tree size is to specify a minimum number of instances that must be present in a leaf. Branches that have a lot of data will keep growing, and those that have fewer data will be limited. This way the tree adapts to the underlying information distribution. When this number is small we might need to adjust the probability estimation with the Laplace correction also called [additive smoothing](https://en.wikipedia.org/wiki/Additive_smoothing).

### Notes and Tips

For an exhaustive list the [scikit-learn docs](https://scikit-learn.org/stable/modules/tree.html#tips-on-practical-use) are a great start.

Continuous features are typically discretised, and the splitting points chose as to maximise the information gain. When the target variable is continuous we can use the variance in place of information gain. The idea is that high variance corresponds means the value is uncertain (low purity) and vice versa.

**Balanced classes**

Decision tree learners create biased trees if some classes dominate. It is best to balance the dataset before fitting.

**Training and Evaluation Complexity**

The order in which we select features matters. To find the optimal tree we would need to try out all permutations, which means we have an NP-complete problem. Suboptimal approaches are typically used, such as greedy algorithms (taking the best decision at each node regardless of what we decided before or what's coming after), and random feature selection. In the following:

- **N** is the number of samples in the dataset
- **f** is the number of features we use to induce the model
- **d** is its depth of the tree

The query time for a decision tree is proportional to the time it takes to traverse the tree (depth). For binary trees this is O(log(N)). If you are familiar with tree-based database indexes this scaling should sound obvious.

The training cost to construct a balanced binary tree is O(f*N*d). This is equivalent to O(f*N*log(N)). For a balanced binary tree with N=16 we have a maximum of 16 leafs, corresponding to a depth of d=log(N)=4. For each split we need to calculate the IG that each of the f features brings using each sample s. Unbalanced trees will be deeper than log(N) and [d=N in the worst-case scenario](https://www.quora.com/How-do-I-calculate-the-time-complexity-of-a-decision-tree-machine-learning-algorithm).

**Trees tend to overfit**

Decision trees tend to overfit on data with a large number of features. A fitting graph can help find the right number of features given the sample size.

If you can give up interpretability, you can reduce the number of features with **Principal Component Analysis (PCA)** or **Independent Component Analysis (ICA)**. Both these algorithms create a new set of features by linearly combining the original ones and differ in what they optimise for: PCA maximises variance, ICA maximises independence.

If you need interpretability you might want to use feature selection instead. In scikit-learn this is typically done as part of a pipeline [before we learn the model](https://scikit-learn.org/stable/modules/feature_selection.html#feature-selection-as-part-of-a-pipeline), using various methods from recursive feature elimination, to using other estimators to rank the feature importance and only focus on those that are above a threshold. For more details see the official docs on [feature selection](https://scikit-learn.org/stable/modules/feature_selection.html#feature-selection-as-part-of-a-pipeline).

### Comparison with other linear models

Decision trees use linear decision boundaries just as other linear models. However, boundaries are perpendicular to the instance-space axes, because features are selected one at a time. In contrast, the boundary of a linear classifier can have any orientation. However, trees can slice the feature space in as many parts as there are nodes, while linear models can only generate two segments.

### More Reading
- [Wisconsin Breast Cancer dataset](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+(Diagnostic)
- [Trees for regression](https://scikit-learn.org/stable/modules/tree.html#regression)
- [Multi-output problems](https://scikit-learn.org/stable/modules/tree.html#regression)
- [Random forest in Python](https://towardsdatascience.com/an-implementation-and-explanation-of-the-random-forest-in-python-77bf308a9b76)
