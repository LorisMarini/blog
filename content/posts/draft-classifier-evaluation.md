---
title: "Evaluating classifiers"
subtitle: ""
date: 2020-03-20T14:00:00+10:00
featured: false
draft: true
---

Checkout sklearn [scoring parameters](https://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter).

Then check `cross_validate` and the scorer type it accepts. The full list of scorers available is here:

```
import sklearn
sklearn.metrics.SCORERS
```

**Why accuracy sucks**
[Accuracy Paradox](https://en.wikipedia.org/wiki/Accuracy_paradox)

See also this example from [Google ML crash course](https://developers.google.com/machine-learning/crash-course/classification/accuracy).

https://stats.stackexchange.com/questions/312780/why-is-accuracy-not-the-best-measure-for-assessing-classification-models
> When we use accuracy, we assign equal cost to false positives and false negatives. When that data set is imbalanced - say it has 99% of instances in one class and only 1 % in the other - there is a great way to lower the cost. Predict that every instance belongs to the majority class, get accuracy of 99% and go home early.

https://scikit-learn.org/stable/modules/model_evaluation.html#dummy-estimators
> More generally, when the accuracy of a classifier is too close to random, it probably means that something went wrong: features are not helpful, a hyperparameter is not correctly tuned, the classifier is suffering from class imbalance, etc…
