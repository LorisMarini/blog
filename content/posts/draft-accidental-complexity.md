---
title: "Accidental Complexity?"
subtitle: ""
date: 2020-04-29T10:30:23+10:00
featured: false
draft: true
tags:
---


Distinction between essential and accidental complexity! https://medium.com/darklang/the-design-of-dark-59f5d38e52d2

[Darklang](https://darklang.com/)
[What is Dark](https://medium.com/darklang/the-design-of-dark-59f5d38e52d2)

- Darklang
- Cortex
