---
Title: "Publications"
subPaper: "Peer-reviewed papers"
date: 2020-04-06T9:30:00+10:00
featured: false
draft: false
---

**2019 - Changing the colour of light in Silicon**

This paper describes a frequency-conversion experiment of two laser pulses using a nonlinear optical phenomenon called four-wave mixing (FWM). Light interacts inside microscopic silicon disks designed to resonate with the frequency of the two pulses. This double resonance leads to 100x increase in conversion efficiency compared to plain silicon films.

![changing_the_colour_of_light_in_silicon](/images/changing_the_colour_of_light_in_silicon.png)

- Paper: [Enhanced Four-Wave Mixing in Doubly Resonant Si Nanoresonators](https://pubs.acs.org/doi/10.1021/acsphotonics.9b00442)
- People: R. Colom, L. Xu, L. Marini et. al.
- Journal: [ACS Photonics](https://pubs.acs.org/journal/apchd5)

**2018 - Single-photons from material imperfections**

One way to generate [qbits](https://www.technologyreview.com/2019/01/29/66141/what-is-quantum-computing/) is by mixing together identical single photons. However, photons really like company and isolating singles is harder that you might think. One way to do this is to perturb a periodic atomic structure (crystal) by either introducing impurities or by bombarding it to knock away some of its atoms. This work explores the first option, in mono-atomic layers of hexagonal Boron Nitride. Other than being a great lubricant for your bike's chain, hBN crystals are relatively easy to peel off into single layers and be used in an optics lab!

![hbn_paper_setup](/images/hbn_paper.png)

- Paper: NIR Quantum Emission from hBN
- People: L.Marini, [R. Champhausen](https://www.linkedin.com/in/robin-camphausen-25a6ba116/), [S. Palomba](https://www.linkedin.com/in/stefano-palomba-5921662a/)
- Journal: [APL Photonics](https://aip.scitation.org/journal/app)

**2017 - Qbits for Quantum Computers**

Another way to generate [qbits](https://www.technologyreview.com/2019/01/29/66141/what-is-quantum-computing/) is with a phenomenon called spontaneous parametric downconversion (SPDC), which has been measured extensively in large structures but never observed directly in atomically-thick crystals. This work combines theory and data from high-power laser experiments and questions the practical usefulness of 2D materials for the generation of entangled states of light.

- Paper: [Constraints on Downconversion in Atomically Thick Films](https://www.osapublishing.org/josab/abstract.cfm?uri=josab-35-4-672)
- People: L. Marini, [L. Helt](https://www.linkedin.com/in/luke-helt-6236282/), [B.J. Eggleton](https://www.linkedin.com/in/benjamin-eggleton-4250127/), [S. Palomba](https://www.linkedin.com/in/stefano-palomba-5921662a/)
- Journal: [JOSAB](https://www.osapublishing.org/josab/home.cfm)

**2017 - Single-photon selfies**

A full moon in the outback reflects [a hundred-thousand billion visible photons on each square meter](https://www.researchgate.net/figure/Full-Moon-irradiance-above-the-atmosphere-vs-wavelength-Left-Log-linear-plot-Right_fig1_323029152) of land. So if we want to be able to detect even a single photon, we need some serious engineering. One way to do this is to create a harsh electrical environment (p-n junctions) where even the tiniest change in energy can be multiplied many times until it gets out of control (avalanche) and a current is detected by an electronic circuit. After a detection, some electrons may be reabsorbed and release energy in the form of light particles. This is called breakdown-flashing and is similar to taking a selfie with your flash-on in front of a mirror. This work described the phenomenon and shows how to mitigate it.

- Paper: [Deterministic Filtering of Breakdown Flashing at Telecom Wavelengths](https://aip.scitation.org/doi/10.1063/1.4997333)
- People: L.Marini, [R. Champhausen](https://www.linkedin.com/in/robin-camphausen-25a6ba116/), [B.J. Eggleton](https://www.linkedin.com/in/benjamin-eggleton-4250127/), [S. Palomba](https://www.linkedin.com/in/stefano-palomba-5921662a/)
- Journal: [Applied Physics Letters](https://aip.scitation.org/journal/apl)

**2016 - Distributed caching with Reinforcement Learning**

- Paper: [Learning Automation based Distributed Caching for Mobile Social Networks](https://ieeexplore.ieee.org/document/7564817)
- People: C. Ma, L. Marini, J. Li, Z. Lin
- Conference: [IEEE Wireless Communications and Networking Conference](https://wcnc2020.ieee-wcnc.org/)


**2015 - Solving NP-hard problems with Reinforcement Learning**

- Paper: [Distributed Caching based on Decentralized Learning Automata](https://ieeexplore.ieee.org/document/7248917)
- People: L. Marini, [J. Li](https://www.linkedin.com/in/jun-li-a8198022/), [Y. Li](https://www.linkedin.com/in/yonghui-li-b2000b4/)
- Conference: [International Conference on Communications](https://ieeexplore.ieee.org/servlet/opac?punumber=7225357)

**2012 - Interference suppression when everybody speaks at once**

- Paper: [Multi-Beam Satellite MIMO Systems: BER Analysis of Interference Cancellation](https://ieeexplore.ieee.org/document/6333075)
- People: [V. Boussemart](https://www.linkedin.com/in/vincent-boussemart-493342b/), L. Marini, [M. Berioli](https://www.linkedin.com/in/matteo-berioli-8394393/)
- Conference: [Advanced Mobile Satellite Systems](https://ieeexplore.ieee.org/servlet/opac?punumber=6324497)

**2012 - Taking turns to speak in overcrowded rooms**

- Paper: [Impact of Scheduling on the Return-Link of Interference-Limited Multi-Beam Satellite Systems](https://arxiv.org/pdf/1210.4329.pdf)
- People: [V. Boussemart](https://www.linkedin.com/in/vincent-boussemart-493342b/), L. Marini, [M. Berioli](https://www.linkedin.com/in/matteo-berioli-8394393/)
- Conference: IEEE

I hope you enjoyed this post! If you have suggestions or thoughts leave a comment below or reach out on your preferred social media platform. Happy inferencing :).
