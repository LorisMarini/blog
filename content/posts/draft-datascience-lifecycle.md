---
title: "Data Science Lifecycle"
subtitle: ""
date: 2020-03-26T13:30:23+10:00
featured: false
draft: true
---

Data science is an exciting cross-disciplinary effort with many learning opportunities. Making data driven decisions with confidence requires a clear and shared understanding of risks and opportunities, and this can be challenging given the diversity in language and priorities. Aligning the team means requires at least two ingredients:

- culture
- tooling

A good culture encourages collaboration by rewarding knowledge sharing and constructive scepticism in a [blameless fashion](https://landing.google.com/sre/sre-book/chapters/postmortem-culture/). Good tooling should support the team culture.

**Key Points**

1. Successful data science is a collaborative effort between different business domains
2. A shared vocabulary can reduce misunderstandings
3. Data scientist should structure reports with an increasing level of detail. The audience should choose wether they have time for a deep dive or prefer to stick to a one-line summary.
4. The bulk of data science is not modelling, but building datasets and engineering features.

### A shared vocabulary

The marketing team in your firm calculated that acquiring new customers costs 5 times more than retaining them. We want to estimate the churn probability, and strategically [use incentives to convince them to stay](https://www.autopilothq.com/blog/reduce-customer-churn). Let's explore one way of defining the problem. Any measurable system can be represented as a black box with an input an output. For positive quantities, **loss** is simply the ratio of input over the output. Example: input=10 and output=5, loss=2 - no brainer. The inverse of loss is called **gain**, which in this example is 10/5 = 0.5. If quantities are greater than 1 their meaning is straightforward:

- loss > 1 - the system is loosing something
- gain > 1 - the system is gaining something

However, when the ratio is smaller than one the meaning of loss and gain flips, so that when loss < 1 the system is actually gaining something, and gain < 1 means that the system is actually loosing something. One way to avoid confusion is to define clearly what we mean with [churn](https://www.omniconvert.com/what-is/customer-churn-rate/) such as in:

>Churn is calculated as the ratio of customers lost over the total number of customers

A positive byproduct of this is that implicit assumptions become a shared understanding. This could mean clarifying industry-specific terms, collectively converging on the main concepts and how they relate to the business, and advocate so that stakeholders align to this shared pull of conventions.

### Asking the right questions

- What business outcome do we want to achieve?
- What is the mathematical formulation of this problem?
- Do we need a [classifier or a predictor](https://www.fharrell.com/post/classification/)?
- How do we measure the classifier performance (cost and opportunity matrix)?
- Do we have historical data?
- Do we have descriptive features (in the churn example think age, usage, business type, recent logins)?

## Workflow

With a clear business objective, and a shared understanding of the problem and the definition of success, we can start the data science workflow:

1. **gather** the dataset (SQL in warehouse, custom ETL to query a NoSQL data store, etc)
2. **explore** the dataset (distributions, outliers, missing values, correlations, feature dependence)
3. **select** informative variables (feature selection)
4. **model** to discover patterns (probabilistic, clustering, classification, regression)
5. **evaluate** the model usefulness
6. **explain** to stakeholders model  heuristic, cost, and business opportunities

**1. gather**

**2. explore**

**3. select**

**4. build**

Modelling typically requires to decide between supervised or unsupervised learning, and its performance should be at least measured against a base rate model. As part of the building step, we the model on previously unseen data and tune its hyper-parameters

- *Lift*: is your model better than random chance?
- *Generalisation*: how well the model performs on data never seen before
- *Learning curve*: generalisation performance as a function of data volume
- *Fitting graphs*: find the model parameters that achieve the best trade-off between variance and bias.

**5. evaluate**

The usefulness of a model depends the business context. For example, consider a binary classifier used in a fire-evacuation system. If the system reads every hour, a false-positive rate of 1% corresponds to 87.6 unnecessary evacuations every year (If you live in Australia you certainly know how disruptive this can be). However, a similar classifier used to decided wether to show an add to a customer might be acceptable, as the cost per click is typically low, and the application is lossy anyways. One can formalise this by combining the confusion matrix, with the profit and loss matrix.

**6. explain**

Two facts about humans:

1. we are rational and social animals
2. we love simplicity

A very predictable consequence of this is that we tend to forget things we don't understand. Imagine that the subject is an insurance model that a data scientist crafted over the course of a year. How would they feel seeing their hard work being *forgotten* or *ignored* because too complicated? How likely would they be to get a promotion? The fact is that the person deciding wether a model should be in production is almost never the same that built it. Failing to explain what you have done clearly can severely impact how you are perceived by other people in your organisation.

For example, if you only ever give detailed answers an executive might perceive you as unclear, contradictory and undecided (I certainly made this mistake at some point in my career). If decisions are then taken based on gut feeling and they are correct, you as a data scientist will loose credibility. If however turn out to be wrong they can negatively impact the business.

As [Tom Hunt](https://www.linkedin.com/in/thoshunt/?originalSubdomain=au) explained during his latest [Meetup talk](https://www.meetup.com/Data-and-Analytics-Wednesday-Sydney/events/fgqcqpyxnbnb/) there are two crucial questions to keep in mind:

- How do you talk to stakeholders when they won’t pay attention to the detail?
- Is it better to give a white lie that results in the correct decision or blind them with science?

A white lie is:
> An abstraction that allows a stakeholder to not have to think about the context that you are an expert in.

Key takeaways were:

- Don't withhold details, just don't offer it first
- Educate down or horizontally, don't educate up

### A note on tooling

A fundamental engineering principle is that optimality depends on the problem at hand. In a distributed organisation, multi-user, cloud-based editors are useful tools to run meeting notes (Notion, Goole Doc, Dropbox Paper etc). As Matt Mullenweg from [Automatic](https://automattic.com/about/) pointed out in [the new future of work](https://samharris.org/podcasts/194-new-future-work/), real-time meeting notes reinforce a shared understanding and stimulate questions when this is not the case. Another example of good tools is messaging platforms such as Slack, with short-lived and project-focused channels where the relevant team members can collaborate asynchronously, and knowledge management platforms such as Stack Exchange [for teams](https://stackoverflow.com/teams) to mention one.
