---
title: "A static blog with Hugo"
subtitle: ""
date: 2020-05-18T14:00:00+10:00
featured: false
draft: true
---

Up until 1994 websites did not support any type of user interactivity. Except for reloading the page, it was not possible to select options from a drop-down menu, zoom in and out of a map, or consume content. In 1995 Netscape introduced Javascript to their *Navigator* browser to add some dynamism, and 25 years later it's used by [95%](https://w3techs.com/technologies/details/cp-javascript) of the websites.

Like anything, JS isn't a silver bullet. It's slow, it's heavy, and prone to [attacks](https://medium.com/@dhtmlx/security-of-javascript-applications-1c95cd2ce533). When simplicity and performance matter, nothing beats a bunch of HTML plus some CSS for styling - a.k.a. static pages. However, since writing html is not fun, people invented static website generators to produce html from a much more human-friendly, and platform-independent, markup language called markdown.

{{< admonition note "About Markup..." >}}
A markup language is a system for annotating a document in a way that is syntactically distinguishable from the text, meaning when the document is processed for display, the markup language is not shown, and is only used to format the text. There are three kinds of markup languages:
- Presentational, example: Word (WYSIWYG).
- Procedural, example: TeX (embedded in the text and provides instructions to process the text)
- Descriptive, example HTML, LaTeX (explicit labelling of how each part of the document should look like)
{{< /admonition >}}

With [countless](https://www.staticgen.com/) [platforms](https://github.com/myles/awesome-static-generators) out there it doesn't make sense to speak of an absolute best. Rather, a good choice is about weighting strengths and weaknesses.

### Why Hugo

Before Hugo I worked with Jekyll and overall liked it, in fact my [resume](https://www.resume.lorismarini.dev/) uses Jekyll following this [template](https://github.com/LorisMarini/jekyll-resume). I would have probably sticked to Jekyll but the build performance of Hugo are hard to resits. Hugo is built in GoLang, a highly-concurrent, compiled, lightweight, and strictly-typed programming language developed by Google. I was interested and I wanted to try.

### Get Started

1. If you don't have one, create a [GitLab profile](https://gitlab.com/users/sign_up), [install git](https://git-scm.com/downloads) and [hugo](https://gohugo.io/getting-started/quick-start/)
2. Create a new blank project in GitLab:
![](/images/your-hugo-static-blog-6dac0.png)
3. Open terminal and execute these one by one

```zsh
# Move to the folder that will contain your new blog
cd ~/projects

# Init the project (this will create a folder called my-website)
hugo new site my-website

# Move inside the folder
cd my-website

# Initialize a git repository
git init

# Tell git about the remote repo in Gitlab (change link with your details)
git remote add origin git@gitlab.com:<gitlab-username>/<project-name>.git

# Add, commit and push
git add . & git commit -m "Initial commit" & git push -u origin master

# Add a theme as a submodule (we use LoveIt out of many https://themes.gohugo.io/)
git submodule add https://github.com/dillonzq/LoveIt themes/LoveIt

# Add a .gitignore file to avoid committing garbage
echo "# Hugo default output directory
/public

# OSX
.DS_Store" > .gitignore

```
If everything went well, you should have a working git repository on your machine as well as on GitLab. To start working open the current directory with your favourite editor and start adding a post (example: first-post.md) inside content/posts.

### Configuration

Since we are using the LoveIT theme we need to write our configuration in config.toml. You can follow this [gist](https://gist.github.com/LorisMarini/f83aa762051e72e4086e9d12f0aa8173), the documentation, or the examples in the `themes/LoveIt/exampleSite/config.toml`. To serve the website on localhost and see it updating in real time in your browser type:

```zsh
path/to/your/browser "http://localhost:1313/" & \
 hugo server --buildDrafts --disableFastRender
```

This opens a new tab in your browser and fires the hugo server to its default port. To find `path/to/your/browser` for Chrome see [this post](https://www.howtogeek.com/255653/how-to-find-your-chrome-profile-folder-on-windows-mac-and-linux/). I aliased this command to simply `hs` for "hugo serve" in my zsh for convenience. The page should be empty, because there is no content yet. Add a new file in `content/posts/firstpost.md`, type "Hello World!", and save. The hugo server should have detected the change and automatically displayed changes in your browser. LoveIt is a complex theme and there is a lot to learn.

I hope you enjoyed this post! If you have suggestions or thoughts leave a comment below or reach out on your preferred social media platform. Happy inferencing :).
