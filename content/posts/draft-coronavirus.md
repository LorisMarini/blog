---
title: "Growth rate of Covid 19"
date: 2020-03-29T10:04:00+10:00
featured: false
draft: True
---

The spread of an infection is typically described by an exponential function. We are used to think in linear terms, and we fail [understanding exponential growth](https://www.youtube.com/watch?v=Kas0tIxDvrg&feature=youtu.be) even when we think we do. This post tries to answer a simple question:

> *How does the spread of COVID19 in Australia compare with Italy?*

### Key Points
- Confirmed cases over time are well described by an "S" curve
- The public restrictions that Italy adopted around mid March 2020 reduced the growth rate of the virus considerably.
- It's is clear from the data when countries run out of ICU units

### Analysis
Checkout the project [repo](https://github.com/LorisMarini/coronavirus) or [explore the analysis in Binder](https://mybinder.org/v2/gh/LorisMarini/coronavirus/master).

![Positive Predictions - Italy](https://raw.githubusercontent.com/LorisMarini/coronavirus/master/covid19-italy-positives-forecast.png)

![Growth rate - Italy](https://raw.githubusercontent.com/LorisMarini/coronavirus/master/covid19-italy-growth-rate.png)

![Infection, Mortality and ICU rates - Italy ](https://raw.githubusercontent.com/LorisMarini/coronavirus/master/covid19-italy-rates.png)

The infection rate increases over time because testing has gradually been restricted to patients with more severe symptoms, more likely to test positive.

### Datasets
- Italy: [Protezione Civile](https://github.com/pcm-dpc/COVID-19)
- Australia: [WHO](https://github.com/CSSEGISandData/COVID-19)
