---
title: "Logistic Regression"
subtitle: "A linear fit to the log-odds function"
date: 2020-03-26T13:30:23+10:00
featured: false
draft: true
tags:
  - machine learning
  - logistic regression
  - statistics
  - fitting
---

Logistic regression is the idea of fitting a line to the natural log of the odds. Odds are defined as p/(1-p) where p is the probability of the event to happen and (1-p) the probability that will not happen. ln(p/(1-p)) = a + bp. The aim is to find `a` and `b` coefficients that minimise the mean square error (MSE).

Many machine learning experts consider logistic regression to be a classification method [it is not](https://www.fharrell.com/post/classification/). However logistic regression can be used as a classifier if we decide that class probability estimations above a threshold indicate a sample belonging to one category or the other. The difference is subtle, but it encodes the fundamental difference between a statistician and a machine learning practitioner: the first tends to think in terms of probabilities and stochastic processes, while the second tends to be more black and white.

**Learning Type**: Supervised
**Inputs**:
**Outputs**:
**Assumptions**:
**When**
**Limitations**
Only 2 classes


## Class Probability Estimation
Logistic regression is used when you want to estimate the probability of an observable to belong to one class of the other of a discrete target vector. **Why** is it called logistic **regression** then?

> because the model produces a numeric estimate (the estimation of the log-odds). However, the values of the target variable in the data are categorical. Debating this point is rather academic. What is important to understand is what logistic regression is doing. It is estimating the log-odds or, more loosely, the probability of class membership (a numeric quantity) over a categorical class. ”
>"Fortunately, within this same framework for fitting linear models to data, by choosing a different objective function we can produce a model designed to give accurate estimates of class probability. The most common procedure by which we do this is called logistic regression.”
Excerpt From: Foster Provost and Tom Fawcett. “Data Science for Business.” Apple Books.

## Odds, Log-odds and Sigmoid curve

odds: p/(1-p)
log-odds: log(p/(1-p))
sigmoid: p(x) = 1/(1+e^-f(x))

```P = list(np.linspace(start=0.001, stop=0.999, num=1000))
log_odds = [np.log(p/(1-p)) for p in P]
plot = pd.Series(index=P, data=log_odds).plot(title="Log-Odds function")

F = list(np.linspace(start=-10, stop=10, num=2000))
p = [1/(1+np.exp(-f))for f in F]
plt.figure()
plot2 = pd.Series(index=F, data=p).plot(title="Sigmoid function")```
