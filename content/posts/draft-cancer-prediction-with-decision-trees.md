---
title: "Cancer prediction with linear models"
subtitle: "The 1991 Wisconsin cancer dataset"
date: 2020-03-26T13:30:23+10:00
featured: false
draft: True
tags:
  - machine learning
  - decision trees
  - information
  - entropy
---

Imagine we have images of breast cancer cells (benign and malignant), which we used to automatically extract features such as cells size, shape etc. The question is:

> Can we predict malignant cancer cells knowing the geometric properties of the cell nuclei?

In this short article we'll use a well known dataset and try to reproduce the accepted findings in python exploring a range of different classifiers.

**What's a decision tree?**

You might recall from this [this post]({{< ref "decision-trees.md" >}}) that a **tree** is a graph made of nodes connected by edges, similar to a flowchart. A **decision tree** is a special type of tree, containing a series of hierarchical binary decisions such as "if variable X is above a threshold take the left, otherwise the right". When read top-to-bottom, a decision tree provides a clear sequence of logical conditions leading to the final classification.

**Data Source**

This post is based on the Wisconsin Breast Cancer Database published by Dr. William H. Wolberg on January 8, 1991 and [available here](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+(Diagnostic)). Breast cells were collected from patients with a technique known as fine needle aspirate (FNA) and analysed under a microscope. A number of features were computed from their images and their state (benign, malignant) recorded in a target vector. See for example [Breast Cancer Diagnosis and Prognosis Via Linear Programming](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.45.8794&rep=rep1&type=pdf).
While classification models for early cancer detection improved greatly since 1991, the field is still under intense research. A good starting point for those interested is this [Nature outlook](https://www.nature.com/articles/d41586-020-00847-2).

**Code**

The code for this post is based on scikit-learn, pandas and matplotlib and is available on [github](https://github.com/LorisMarini/cancer_wisconsin). You can view the notebooks through [nbviewer](https://nbviewer.jupyter.org/github/LorisMarini/cancer_wisconsin/tree/master/) or spawn a dev environment in [binder](https://mybinder.org/v2/gh/LorisMarini/cancer_wisconsin/master).

### How do you build a tree in Python?

Use [graphviz](https://graphviz.readthedocs.io/en/stable/manual.html), which encodes the tree in the [DOT](https://www.graphviz.org/doc/info/lang.html) language, and allows you to visualise it in a Jupyter notebook, or with any other tool such as [gephi](https://gephi.org/):

{{< highlight python >}}
  from graphviz import Digraph
  # Create Digraph object
  dot = Digraph()
  # Add nodes 1 and 2
  dot.node('1')
  dot.node('2')
  # Add edge between 1 and 2 and show graph
  dot.edges(['12'])
  dot
{{< /highlight >}}

The method [tree.graphviz](https://scikit-learn.org/stable/modules/generated/sklearn.tree.export_graphviz.html) generates a GraphViz representation of the decision tree. When we pass `out_file=None`, it returns a string containing the representation of the graph in the DOT language.

### Cross-validaton for a classifier
