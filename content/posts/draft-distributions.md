---
title: "Distribution Functions"
subtitle: "And links between models"
date:
featured: false
draft: true
tags:
  - statistics
  - feature selection
  - fitting
---

[F-distribution](https://en.wikipedia.org/wiki/F-distribution)
  - Used in ANOVA, [F-test](https://en.wikipedia.org/wiki/F-test)
  - F-test estimate the degree of linear dependency between two random variables.

[Chi2 distribution](https://en.wikipedia.org/wiki/Chi-squared_distribution)

[Normal distribution](https://en.wikipedia.org/wiki/Normal_distribution)

[Poisson](https://en.wikipedia.org/wiki/Poisson_distribution)

[Feature selection](https://scikit-learn.org/stable/modules/feature_selection.html)
