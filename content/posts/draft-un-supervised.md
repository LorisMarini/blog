---
title: "Supervised yes or no?"
subtitle: ""
date: 2020-03-26T13:30:23+10:00
featured: false
draft: true
tags:
  - machine learning
  - supervised learning
  - unsupervised learning
---

**Supervised or Unsupervised**

*supervised methods*
Classification, regression, and causal modelling
*unsupervised*
Clustering, co-occurrence grouping, and profiling
*both*
Similarity matching, link prediction, and data reduction could be either
