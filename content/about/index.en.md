---
title: "About"
draft: false
lightgallery: true
math:
  enable: true
---

An engineer and a scientist.
